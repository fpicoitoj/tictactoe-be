const GameModel = require("./model");
const winnerUtils = require("./utils/winner");
const { X, O } = require("./../../utils/consts");

class Controller {
  /**
   * Get the total number of games
   */
  getTotal() {
    return GameModel.count().exec();
  }

  /**
   * Get a particular game
   *
   * @param {ObjectId} id
   */
  get(id) {
    return GameModel.findById(id).exec();
  }

  /**
   * Create a new game
   */
  create() {
    // Given this is tic-tac-toe we can safely
    // fill out a board here.
    // But why not in Model?
    // Because even though it's tic-tac-toe what if later
    // it evolved to something else?
    // Many more changes would be involved, correct.
    // However injecting this kind of data is better than defaulting it.
    const data = {
      board: Array(9).fill(null)
    };
    return new GameModel(data).save();
  }

  /**
   * Update a new game
   *
   * @param {ObjectId} id
   * @param {Object} data
   */
  update(id, data) {
    return GameModel.findByIdAndUpdate(id, data, { new: true }).exec();
  }

  async makeMove(id, { position }) {
    const game = await this.get(id);
    const newBoard = [...game.board];

    // we can't play on already played tiles
    if (newBoard[position] !== null) return game;

    // mark the move
    newBoard[position] = game.isXPlaying ? X : O;

    const updatedGame = await this.update(id, {
      board: newBoard,
      isXPlaying: !game.isXPlaying
    });

    const numberOfMoves = newBoard.filter(b => b !== null).length;

    // if > 4 moves then we can check winner
    if (numberOfMoves > 4) return await this.checkWinner(id);

    return updatedGame;
  }

  /**
   * Checks if we have a conclusion to the game (a winner)
   *
   * @param {ObjectId} id
   */
  async checkWinner(id) {
    const game = await this.get(id);

    // we already have a winner, move along
    if (game.winner) return game;

    const winner = winnerUtils.checkWinner(game.board);

    // if we have a winner, update game with it and return updated game
    if (winner) {
      // writes key for the winner
      const winnerKey = winner === -1 ? "draws" : `${winner.toLowerCase()}Wins`;
      const updatedGame = await this.update(id, {
        winner,
        [winnerKey]: game[winnerKey] + 1
      });

      return updatedGame;
    }

    return game;
  }

  /**
   * Rematches a game (same game, but new match, so winners etc persist)
   *
   * @param {ObjectId} id
   */
  async rematch(id) {
    const game = await this.get(id);

    const rematch = {
      board: Array(9).fill(null),
      isXPlaying: game.winner === O,
      winner: null
    };

    const updatedGame = await this.update(id, rematch);

    return updatedGame;
  }
}

module.exports = new Controller();
