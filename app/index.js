const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");

// Modules
const Game = require("./modules/game");

/**
 * Organize app bootup with class for better readability
 */
class App {
  constructor() {
    this.express = express();
    this.middleware();
    this.database();
    this.routes();
  }

  middleware() {
    // Given the complexity of some configuration we ought to
    // place them in a separate file
    this.express.use(
      cors({
        origin: process.env.CORS_ORIGIN,
        optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
      })
    );
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }

  database() {
    mongoose.Promise = global.Promise; // use built-in Promise
    mongoose.connect(process.env.DATABASE_URL);
  }

  routes() {
    const apiPathV1 = "/api/v1";

    // Hello world for API instead of ugly Cannot GET /
    this.express.get("/", (req, res) => {
      res.send("Hello from tic-tac-toe!");
    });

    this.express.use(`${apiPathV1}/game`, Game);
  }
}

module.exports = new App().express;
