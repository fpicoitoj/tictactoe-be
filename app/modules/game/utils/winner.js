// possible winning combos
const combos = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6]
];

/**
 * Check if we have winner on a given board
 *
 * @param {Array} board
 */
const checkWinner = board => {
  // filled slots (empty slots are null)
  const totalNumberOfPlayerMoves = board.filter(b => b !== null).length;

  // only worth iterating if we have more than 4 moves
  if (totalNumberOfPlayerMoves > 4) {
    for (let combo of combos) {
      // check if all board combo slots belong to the same player
      if (
        board[combo[0]] &&
        board[combo[0]] === board[combo[1]] &&
        board[combo[0]] === board[combo[2]]
      ) {
        return board[combo[0]];
      }
    }
  }

  // if we reach this line with 9 moves it means we have no winner
  if (totalNumberOfPlayerMoves === 9) {
    return -1;
  }

  // default to null if we have no conclusion
  return null;
};

module.exports = {
  checkWinner
};
