const mongoose = require("mongoose");

// Why use class?
// Because we can organize data in a very readable way!
// We could for instance include a Triggers function.
class GameModel {
  constructor() {
    this.schema = this.schema();
    // this.triggers()
  }

  schema() {
    return new mongoose.Schema({
      // this is literally the board, an array with "X", "O" and null
      // we will use null to filter out unplayed tiles
      board: { type: Array, required: true },

      // a very simple way to know which letter is next
      isXPlaying: { type: Boolean, default: true },

      // statistics
      xWins: { type: Number, default: 0 },
      oWins: { type: Number, default: 0 },
      draws: { type: Number, default: 0 },

      // will help determine if there is already a winner
      winner: { type: String, default: null }
    });
  }

  // triggers () {
  //   // do stuff here
  // }
}

module.exports = mongoose.model("Game", new GameModel().schema);
