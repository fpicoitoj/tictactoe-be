const Router = require("./router");

/** 
 * Modules are a way to organize big projects
 * I use them even for small ones as I find it neatier
 * In this simple case we only expose router
 * but we could for instance place workers here
 * or configuration in case of mailers
*/
class Main {
  constructor() {
    this.router = Router;
  }
}

module.exports = new Main().router;
