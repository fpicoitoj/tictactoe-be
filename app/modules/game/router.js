const express = require("express");
const GameController = require("./controller");

class Router {
  constructor() {
    this.router = express.Router();
    this.init();
  }

  /**
   * Get total number of games
   *
   * @param {*} req
   * @param {*} res
   */
  async getTotal(req, res) {
    try {
      const total = await GameController.getTotal(req.params.id);
      return res.status(200).json(total);
    } catch (err) {
      return res.status(500).json({ code: "GET.GAME", message: err.message });
    }
  }

  /**
   * Get game
   *
   * @param {*} req
   * @param {*} res
   */
  async get(req, res) {
    try {
      const game = await GameController.get(req.params.id);
      return res.status(200).json(game);
    } catch (err) {
      return res.status(500).json({ code: "GET.GAME", message: err.message });
    }
  }

  /**
   * Create a game
   *
   * @param {*} req
   * @param {*} res
   */
  async create(req, res) {
    try {
      const game = await GameController.create();
      return res.status(200).json(game);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "CREATE.GAME", message: err.message });
    }
  }

  /**
   * Update a game
   *
   * @param {*} req
   * @param {*} res
   */
  async update(req, res) {
    try {
      const game = await GameController.update(req.params.id, req.body);
      return res.status(200).json(game);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "UPDATE.GAME", message: err.message });
    }
  }

  /**
   * Make a move such as play "X"
   *
   * @param {*} req
   * @param {*} res
   */
  async makeMove(req, res) {
    try {
      const game = await GameController.makeMove(req.params.id, req.body);
      return res.status(200).json(game);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "MAKEMOVE.GAME", message: err.message });
    }
  }

  /**
   * Same as playing another match, the score remains
   *
   * @param {*} req
   * @param {*} res
   */
  async rematch(req, res) {
    try {
      const game = await GameController.rematch(req.params.id);
      return res.status(200).json(game);
    } catch (err) {
      return res
        .status(500)
        .json({ code: "REMATCH.GAME", message: err.message });
    }
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.get("/get-total", this.getTotal);
    this.router.get("/:id", this.get);
    this.router.post("/", this.create);
    this.router.put("/:id", this.update);
    this.router.post("/:id/make-move", this.makeMove);
    this.router.post("/:id/rematch", this.rematch);
  }
}

module.exports = new Router().router;
